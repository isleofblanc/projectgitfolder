﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LiquidVolumeFX;
using UnityEngine.SceneManagement;

public class WaterControl : MonoBehaviour {

	public GameObject water;
	public GameObject waterDrop;
	private float waterLine;
	private RectTransform rt;
	public LiquidVolume lv;
	private bool hasClear = false;
	//public GameObject leaveButton;

	// Use this for initialization
	void Start () {
		waterLine = 0;
		rt = water.GetComponent<RectTransform> ();
		SetWater (waterLine);



	}
	
	// Update is called once per frame
	void Update () {


	}

	public void SetWater (float wl){
		//rt.rect.height = wl;
		Debug.Log("SetWater with waterline " + wl);
		if (wl <= 0.1f) {
			lv.level = 0.1f;
			
		} else {
			lv.level = wl/15;
		}
			
	}

	public void Calculate (float waterAmount){
		float testLine = waterLine + waterAmount;

		Debug.Log("testLine " + waterLine);

		if (testLine > 15){
			waterLine = 0;
			SetWater (waterLine);
			Debug.Log("passed 15! " + waterLine);
			
		}else{
			waterLine = testLine;
			SetWater (waterLine);
			Debug.Log("waterline " + waterLine);

			if (waterLine == 7.0) {
				waterDrop.SetActive (true);
				Debug.Log ("Yeah!You get the water drop!");

				hasClear = true;
				//leaveButton.SetActive (true);

			}
			
		}
	}


}
