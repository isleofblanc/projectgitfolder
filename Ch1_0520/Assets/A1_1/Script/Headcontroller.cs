﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class Headcontroller : MonoBehaviour
{
	public Rigidbody2D rb;
	public float movestep;
	public Camera cam;
	public NavMeshAgent agent;
	public Vector3 centerPos;
	public Image Oar;
	public Vector3 outPos;


	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		float distance_playerdummy = Vector3.Distance (centerPos, agent.transform.position);
		if (distance_playerdummy < 15) {
			Oar.enabled = false;
		}

		float distance_out = Vector3.Distance(outPos, agent.transform.position);
		if(distance_out < 5){
			SceneManager.LoadScene("A1_4a");
		}

		if (Input.GetMouseButton (0)) {
			Ray ray = cam.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit)) {

				float d = Vector3.Distance (hit.point, agent.transform.position);
				if (d < 30.0f) 
				{
					agent.SetDestination (hit.point);
				}

			}
		}
	}


	public void OnPointerDown(PointerEventData eventData){

		/*Vector2 moveAmount;  

		if (transform.position.x >= Input.mousePosition.x) {

			moveAmount.x = -1;
		} else {

			moveAmount.x = 1;


		}


		if (transform.position.y >= Input.mousePosition.y) {

			moveAmount.y = -1;
		} 

		else {

			moveAmount.y = 1;

		}
		transform.position = new Vector2(transform.position.x + moveAmount.x * movestep, transform.position.y + moveAmount.y * movestep);
*/
	}
  


}
