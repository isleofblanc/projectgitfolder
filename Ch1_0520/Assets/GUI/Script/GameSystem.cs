﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSystem : MonoBehaviour {


	public static bool s_soundPlay = true;
	public static bool s_musicPlay = true;
	//public static int s_playSceneIndex = 0;
	Scene m_Scene;

	public GameObject mainMenu;
	public GameObject settingUI;
	//public GameObject infoPage;


	public enum GraphicType
	{
		High,
		Medium,
		Low
	}

	GraphicType graphicType = GraphicType.High;


	public enum Language{
		Chinese,
		English,
	}

	Language language = Language.Chinese;


	void Awake(){
	}

	void Update(){
		m_Scene = SceneManager.GetActiveScene();
	}


	public void setDataBySettingUI(){
		Toggle soundToggle = GameObject.Find ("SoundToggle").GetComponent<Toggle> ();
		GameSystem.s_soundPlay = soundToggle.isOn;

		Toggle musicToggle = GameObject.Find ("MusicToggle").GetComponent<Toggle> ();
		GameSystem.s_musicPlay = musicToggle.isOn;


		Debug.Log ("setting saved");

	}


	public void setSound(){
		Toggle soundToggle = GameObject.Find ("SoundToggle").GetComponent<Toggle> ();
		GameSystem.s_soundPlay = soundToggle.isOn;

		Debug.Log ("sound setting changed");

	}


	public void setMusic(){
		Toggle musicToggle = GameObject.Find ("MusicToggle").GetComponent<Toggle> ();
		GameSystem.s_musicPlay = musicToggle.isOn;

		Debug.Log ("music setting changed");

	}


	public void ChangeGraphic(int index){

		ChangeGraphic ((GraphicType)index);
	}

	public void ChangeGraphic(GraphicType type){
		if (graphicType == type)
			return;

		graphicType = type;
		switch (type) 
		{
		case GraphicType.High:
			Debug.Log ("Graphic High");
			break;
		case GraphicType.Medium:
			Debug.Log ("Graphic Medium");
			break;
		case GraphicType.Low:
			Debug.Log ("Graphic Low");
			break;
		default:
			break;
		}
	}



	public void ChangeLan(int index){

		ChangeLan ((Language)index);
	}

	public void ChangeLan(Language type){

		if (language == type)
			return;

		language = type;
		switch (type) {
		case Language.Chinese:
			Debug.Log ("Jiang Zhong Wen!");
			break;
		case Language.English:
			Debug.Log ("Speak English!");
			break;
		default:
			break;
		}

	}

	public void Reset() {

		//SceneManager.LoadScene (m_Scene.name);
		SceneManager.LoadScene ("A1_map");
		Debug.Log ("Reset active scene A1 map.");
	}


	public void GoToMain(){
		mainMenu.SetActive(true);
		Debug.Log ("Go to the Main Menu!");
	}

	public void BackToGame(){

		settingUI.SetActive (false);
		Debug.Log ("Back to the game.");
	}


	public void PushLeaveButton(){
		SceneManager.LoadScene ("A1_map");
	}	


}
