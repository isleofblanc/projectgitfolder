﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene1Controller : MonoBehaviour
{
    public Transform MyTree;
    public Transform MyPineNut;
    public Transform MySquirrel;
    public GameObject TipsDialog;
    public GameObject MyFlower;
    public GameObject MyScissor;
    public Transform MyWater;
    [HideInInspector]
    public AudioSource MyScene1Audio;
    [HideInInspector]
    public bool IsClickKey = false;
    public AudioClip MyScene1_1Audio;
    public AudioClip MyScene1_2Audio;
    public Transform[] transformArray;
    public Transform[] correctTransformArray;
    [HideInInspector]
    public bool isDrag = false;

    private Vector3[] posBackupArray;
    private bool isExistedPineNut=false;
    


    void Start ()
    {
        MyScene1Audio = transform.GetComponent<AudioSource>();
        backupPos();
    }
	
	void Update ()
    {
		
	}

    public void OnClickTree()
    {
        Debug.Log("click pine nut continue");
        MyTree.GetComponent<Animator>().SetBool("IsTree", true);
    }
    public void OnClickPineNut()
    {
        Debug.Log("click squirrel continue");
        MyTree.GetComponent<Animator>().SetBool("IsTree", false);
        isExistedPineNut = true;
    }

    public void OnClickSquirrel()
    {
        if (isExistedPineNut==false)
        {
            TipsDialog.SetActive(true);
        }
        else
        {
            Debug.Log("click key continue");
            MyScene1Audio.clip = MyScene1_1Audio;
            MyScene1Audio.Play();
            MyTree.GetComponent<Animator>().SetBool("IsEatNut", true);
            MySquirrel.GetComponent<Animator>().SetBool("Squirrel", true);
            IsClickKey = true;
        }   
    }

    public void CloseTipsDialog()
    {
        TipsDialog.SetActive(false);
    }

    public AudioSource MyAS;
    public int[] correctOrderArray;
    private int currentOrderIndex = 0;

    public void PlaySound(AudioClip ac)
    {
        MyAS.clip = ac;
        MyAS.Play();
    }

    public bool checkResult(int soundValue)
    {
        if (soundValue == correctOrderArray[currentOrderIndex])
        {
            currentOrderIndex++;

            if (currentOrderIndex == correctOrderArray.Length)
            {
                Debug.Log("Success! click flower continue");
                MyWater.GetChild(0).gameObject.SetActive(true);
                MyFlower.SetActive(true);
                MyFlower.GetComponent<Animator>().SetBool("IsFlower", true);
                return true;
            }
        }
        else
        {
            currentOrderIndex = 0;
        }
        return false;
    }

    public void OnClickFlower()
    {
        MyScene1Audio.clip = MyScene1_2Audio;
        MyScene1Audio.Play();
        isDrag = true;
        Debug.Log("drag key continue");
    }

    private void backupPos()
    {
        posBackupArray = new Vector3[transformArray.Length];
        for (int i = 0; i < transformArray.Length; ++i)
        {
            posBackupArray[i] = transformArray[i].position;
        }
    }

    private int getTransformArrayIndexByT(Transform t)
    {
        for (int i = 0; i < transformArray.Length; ++i)
        {
            if (t == transformArray[i])
            {
                return i;
            }
        }

        return -1;// return-1????
    }

    private void swapTransformArrayByIndex(int i1, int i2)
    {
        Transform t1 = transformArray[i1];
        Transform t2 = transformArray[i2];
        transformArray[i1] = t2;
        transformArray[i2] = t1;
    }

    private bool CheckResult()
    {
        bool success = true;
        for (int i = 0; i < transformArray.Length; ++i)
        {
            if (transformArray[i] != correctTransformArray[i])
            {
                success = false;
                break;
            }
        }

        return success;

    }

    public void DropImage(Transform t)
    {
        for (int i = 0; i < transformArray.Length; ++i)
        {
            if (t == transformArray[i])
            {
                continue;
            }

            RectTransform rectTransform = transformArray[i].GetComponent<RectTransform>();
            Rect rect = rectTransform.rect;

            float w = Mathf.Abs(t.position.x - transformArray[i].position.x);
            float h = Mathf.Abs(t.position.y - transformArray[i].position.y);

            Debug.Log("w"+ w);
            Debug.Log("h" + h);
            Debug.Log("ww" + rect.width / 2);
            Debug.Log("hh" + rect.height / 2);
            if (w < rect.width / 2 && h < rect.height / 2)
            {
                int tIndex = getTransformArrayIndexByT(t);
                Vector3 p1 = posBackupArray[tIndex];
                Vector3 p2 = transformArray[i].position;

                t.position = p2;
                transformArray[i].position = p1;
                swapTransformArrayByIndex(tIndex, i);

                backupPos();


                if (CheckResult())
                {
                    Debug.Log("Success!");
                    MyWater.GetChild(1).gameObject.SetActive(true);
                    MyScissor.SetActive(true);

                }
                return;
            }

        }

        for (int i = 0; i < transformArray.Length; ++i)
        {
            if (t == transformArray[i])
            {
                t.position = posBackupArray[i];
            }
        }

    }




}
