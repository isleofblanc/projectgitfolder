﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneControl : MonoBehaviour {

	public GameObject[] objectArray;
	public List<GameObject> objectList;

	public void Test(){
		//遍历

		//objectList运行时可添加成员

	}


	public GameObject buttonTree1;
	public GameObject buttonTree2;
	public GameObject buttonPineNutSlot1;
	public GameObject buttonPineNutSlot2;
	public GameObject buttonScissorSlot;
	public GameObject buttonInventorySlot1;

	public GameObject squirral;
	public Animation squirralEating;
	public Sprite pineNutImage;
	public Sprite inventoryImage;
	public Sprite scissorImage;

	public GameObject flower;

	public GameObject water1;
	public GameObject water2;


	private bool hasPineNut;
	private bool eatPineNut;
	private bool pineNutEaten;
	private bool flowerBloom;

	private AudioSource music1;
	private AudioSource music2;

	// Use this for initialization
	void Start () {
		hasPineNut = false;
		eatPineNut = false;
		pineNutEaten = false;
		flowerBloom = false;
		music1 = squirral.GetComponent<AudioSource> ();
		music2 = flower.GetComponent<AudioSource> ();

		StartCoroutine (UpdateCoutine());
	}

	//1.button->water->call other


	IEnumerator UpdateCoutine()
	{
		Debug.Log("This is Update Coroutine 1");
		yield return null;
		Debug.Log("This is Update Coroutine 2");

		yield return new WaitForSeconds (5);
		Debug.Log("This is Update Coroutine 3");
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void PushButtonTree(){

		if (hasPineNut==false){
		buttonPineNutSlot1.SetActive (true);
		hasPineNut = true;
		}
	}

	public void PushPineNutSlot1(){

		buttonPineNutSlot1.SetActive (false);
		CollectInventory (pineNutImage);
	}


	//move this method to UI script?
	public void CollectInventory(Sprite itemImage){

		buttonInventorySlot1.GetComponent<Image> ().sprite = itemImage;
	}


	public void PushSquirralButton(){

		if (pineNutEaten == false) {

			//play eating animation
			buttonInventorySlot1.GetComponent<Image> ().sprite = inventoryImage;
			buttonPineNutSlot2.SetActive (true);

			eatPineNut = true;

			//play 1st music
			music1.Play();

		}
	}


	public void PushFlowerButton(){


		if (flowerBloom == false) {

			flowerBloom = true;
			music2.Play ();
		}

	}



	public void PushKeyButton(){

		if (eatPineNut == true && pineNutEaten == false) {

			pineNutEaten = true;

			water1.SetActive (true);
			flower.SetActive (true);
		}


		if (flowerBloom == true) {
			water2.SetActive (true);
			buttonScissorSlot.SetActive (true);
		}

	}

	public void PushScissorButton(){

		buttonScissorSlot.SetActive (false);
		CollectInventory (scissorImage);
	}


}
