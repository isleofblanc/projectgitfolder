﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KeySound : MonoBehaviour,IPointerClickHandler,IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    public Scene1Controller m_controller;
    public AudioClip m_AC;
    public AudioClip m_AC2;
    public int soundValue;

    void Start()
    {

    }

    void Update()
    {

    }


    public void OnPointerClick(PointerEventData eventData)
    {
        if (m_controller.IsClickKey == true)
        {
            Debug.Log("PointerClick");
            m_controller.MySquirrel.GetComponent<Animator>().SetBool("Squirrel", false);
            m_controller.PlaySound(m_AC);
            m_controller.checkResult(soundValue);  
        }
        else
        {
            Debug.Log("关卡尚未开放");
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (m_controller.isDrag==true)
        {
            Debug.Log("OnDrag");
            m_controller.IsClickKey = false;
            Vector3 v = Input.mousePosition;
            v.y = transform.position.y;
            transform.position = v;
            //transform.position=Input.mousePosition;//??????
        }
        else
        {
            Debug.Log("关卡尚未开放200");
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (m_controller.isDrag == true)
        {
            Debug.Log("OnPointerDown");
            m_controller.IsClickKey = false;
            Transform oldParent = transform.parent;
            transform.parent = null;
            transform.parent = oldParent;
        }
        else
        {
            Debug.Log("关卡尚未开放2");
        }
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (m_controller.isDrag == true)
        {
            Debug.Log("OnPointerUp");
            m_controller.IsClickKey = false;
            m_controller.PlaySound(m_AC2);
            m_controller.DropImage(transform);
        }
        else
        {
            Debug.Log("关卡尚未开放20");
        }
    }

  
}
