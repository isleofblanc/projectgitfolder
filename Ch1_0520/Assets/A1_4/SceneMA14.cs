﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneMA14 : MonoBehaviour {

	private bool waterDrop = true;
	private bool oarActive = true;
	public GameObject oar;
	public Sprite imgWater;
	public GameObject sign;

	// Use this for initialization
	void Start () {
		//check waterDrop

		if (waterDrop == true) {
			sign.GetComponent<Image> ().sprite = imgWater;


		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PushBoat(){

		//check UI oarActive

		//render oar 
		oar.SetActive(true);

		StartCoroutine (NextScene ());

	}
	IEnumerator NextScene(){

		yield return new WaitForSeconds(3.0f);
		SceneManager.LoadScene("A1_4b");
	}
}
